let currentCell = undefined;
let r = 0;
let g = 0;
let b = 0;
let model = [
    /*7,7,7,7,7,7,7,7,
    7,7,3,3,3,3,7,7,
    3,3,0,3,3,0,3,3,
    7,3,3,3,3,3,3,7,
    7,7,5,5,5,5,7,7,
    7,3,1,5,5,1,3,7,
    7,7,4,4,4,4,7,7,
    7,3,3,2,2,3,3,7*/

 7,7,1,1,1,1,7,7,
 7,1,1,1,1,1,1,7,
 1,5,1,1,1,1,5,1,
 1,4,5,1,1,5,4,1,
 1,0,0,0,0,0,0,1,
 1,5,5,0,0,5,5,1,
 7,7,5,0,0,5,7,7,
 7,7,6,0,0,6,7,7
 ]

let colors = [
    "rgb(0, 0, 0)",
    "rgb(255, 0, 0)",
    "rgb(0, 0, 255)",
    "rgb(0, 255, 0)",
    "rgb(255, 255, 0)",
    "rgb(0, 255, 255)",
    "rgb(255, 0, 255)",
    "rgb(255, 255, 255)",
];

let matrice= [
    'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight',
    'nine','ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen',
    'seventeen','eighteen','nineteen','twenty', 'twenty-one','twenty-two','twenty-three','twenty-four',
    'twenty-five','twenty-six','twenty-seven','twenty-eight','twenty-nine','thirty','thirty-one','thirty-two',
    'thirty-three','thirty-four','thirty-five','thirty-six','thirty-seven','thirty-eight','thirty-nine','forty',
    'forty-one','forty-two','forty-three','forty-four','forty-five','forty-six', 'forty-seven','forty-eight',
    'forty-nine','fifty','fifty-one','fifty-two','fifty-three','fifty-four', 'fifty-five','fifty-six',
    'fifty-seven','fifty-eight','fifty-nine','sixty','sixty-one','sixty-two','sixty-three','sixty-four'
];

let painting = [];

let currentPaintCell = [];


document.querySelector(".container").addEventListener("click", function (e){
    currentCell = e.target.id;

    currentCell == !undefined ? document.querySelector('button#paint').classList.add("displayNone") : document.querySelector('button#paint').classList.remove("displayNone")
    currentCell == !undefined ? document.querySelector('button#reload').classList.add("displayNone") : document.querySelector('button#reload').classList.remove("displayNone")
    document.getElementById(currentCell).style.backgroundColor = "black"; // indique la cellule sélectionnée
    document.getElementById(currentCell).style.opacity = "20%";
    document.querySelector(".container").style.pointerEvents = ("none"); // fige la matrice


})

document.querySelector("button#paint").addEventListener("click", function (){
    r = document.getElementById("slideRed").checked ? 255 : 0; // check si rouge est coché
    g = document.getElementById("slideGreen").checked ? 255 : 0; // check si vert est coché
    b = document.getElementById("slideBlue").checked ? 255 : 0; // check si bleu est coché
    document.getElementById(currentCell).style.background = percentageToRGB(r, g, b); // defini la couleur de la cell en fonction des 3 couleurs
    currentPaintCell.push(window.getComputedStyle(document.getElementById(currentCell)).backgroundColor) // recupere la couleur
    painting[matrice.indexOf(currentCell)] = colors.indexOf(currentPaintCell[currentPaintCell.length-1]) // attribue la couleur dans la position correspondant à la matrice
    document.querySelector(".container").style.pointerEvents = ("initial"); // débloque la matrice
    document.getElementById(currentCell).style.opacity = "100%"; // reinitialise la cellule choisie
    document.getElementById("slideRed").checked = false; // reinitialise le checkbox
    document.getElementById("slideGreen").checked = false; // reinitialise le checkbox
    document.getElementById("slideBlue").checked = false; // reinitialise le checkbox
    painting.length == 64 ? document.querySelector("button#submit").classList.remove("displayNone") : document.querySelector("button#submit").classList.add("displayNone");
    currentCell=undefined
    currentCell == undefined ? document.querySelector('button#paint').classList.add("displayNone") : document.querySelector('button#paint').classList.remove("displayNone")
    currentCell == undefined ? document.querySelector('button#reload').classList.add("displayNone") : document.querySelector('button#reload').classList.remove("displayNone")

})

document.querySelector("button#submit").addEventListener("click", function(){
    console.log(model)
    console.log(painting)
    console.log( arrayEquals(painting, model))
    if (arrayEquals(painting,model)){
        document.querySelector('img.blackstar').classList.add('fall');
    }
    else{
        document.querySelector('img.obiwan').classList.add('die');
    }
})

document.querySelector("button#reload").addEventListener("click", function (){
    document.getElementById(currentCell).style.backgroundColor = "#b3bead"; // indique la cellule sélectionnée
    document.getElementById(currentCell).style.opacity = "100%";
    document.querySelector(".container").style.pointerEvents = ("initial"); // fige la matrice
    currentCell = undefined;
    document.getElementById("slideRed").checked = false; // reinitialise le checkbox
    document.getElementById("slideGreen").checked = false; // reinitialise le checkbox
    document.getElementById("slideBlue").checked = false; // reinitialise le checkbox
    painting = []; // reinitilise le tableau
    currentCell == undefined ? document.querySelector('button#paint').classList.add("displayNone") : document.querySelector('button#paint').classList.remove("displayNone")
    currentCell == undefined ? document.querySelector('button#reload').classList.add("displayNone") : document.querySelector('button#reload').classList.remove("displayNone")

})

// https://stackoverflow.com/questions/33723326/combine-rgb-color-values-in-javascript-to-form-a-blend
function percentageToRGB(r,g,b) {
     r = parseInt((r/100)*255);
     g = parseInt((g/100)*255);
     b = parseInt((b/100)*255);

    return "rgb("+ r +","+ g +","+ b +")";
}

// https://masteringjs.io/tutorials/fundamentals/compare-arrays
function arrayEquals(a, b) {
    return Array.isArray(a) &&
        Array.isArray(b) &&
        a.length === b.length &&
        a.every((val, index) => val === b[index]);
}

